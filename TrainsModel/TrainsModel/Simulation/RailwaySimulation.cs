﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrainsModel.Simulation
{
    public class CollisionException : Exception
    {
        public CollisionException() : base("Detected collision")
        { }
    }

    public class RailwaySimulation
    {
        private readonly RailwayNetwork Map;
        private readonly LinkedList<Train> Trains;

        private LinkedList<Train> ActiveTrains;

        private readonly int DeltaTime;

        public int Time { get { return _Time; } }
        private int _Time;

        public bool AllTrainsArrived { get; private set; }

        public RailwaySimulation(RailwayNetwork map)
        {
            Map = map;
            Trains = new LinkedList<Train>();
            ActiveTrains = new LinkedList<Train>();
            DeltaTime = 1;
        }

        public void AddTrain(Train train)
        {
            train.Map = Map;
            _ = Trains.AddLast(train);
        }

        public void Update()
        {
            if (_Time == 0)
                OnFirstIter();

            foreach (Train train in ActiveTrains)
                train.Update(DeltaTime);

            _Time += DeltaTime;

            DetectCollisions();

            var res = ActiveTrains.Where(delegate (Train train) { return !train.Arrived; });
            ActiveTrains = new LinkedList<Train>(res);

            if (ActiveTrains.Count == 0)
                AllTrainsArrived = true;
        }

        private void OnFirstIter()
        {
            foreach (Train train in Trains)
            {
                ActiveTrains.AddLast(train);
                train.Start();
            }

            DetectCollisions();
        }

        private void DetectCollisions()
        {
            for (LinkedListNode<Train> node_1 = ActiveTrains.First; node_1.Next != null; node_1 = node_1.Next)
                for (LinkedListNode<Train> node_2 = node_1.Next; node_2 != null; node_2 = node_2.Next)
                    if (node_1.Value != node_2.Value && CheckCollision(node_1.Value, node_2.Value))
                        throw new CollisionException();
        }

        private bool CheckCollision(Train first, Train second)
        {
            if (first.LastStation == second.LastStation &&
                first.DistanceFromLastStation == 0 && second.DistanceFromLastStation == 0) // same station case
                return true;

            if (first.Arrived || second.Arrived) // if arrived then cannot be collision on road
                return false;

            if (first.NextStation == second.LastStation && first.LastStation == second.NextStation &&
                (Map.GetDistance(first.LastStation, first.NextStation) - first.DistanceFromLastStation == second.DistanceFromLastStation ||
                 Map.GetDistance(first.LastStation, first.NextStation) - first.DistanceFromLastStation == second.DistanceFromLastStation + 1)) // collision on road
                return true;

            return false;
        }

        public virtual void Reset()
        {
            _Time = 0;
            AllTrainsArrived = false;

            ActiveTrains.Clear();
            foreach (Train train in Trains)
                train.Reset();
        }
    }
}
