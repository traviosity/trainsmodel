﻿using System;
using System.Collections.Generic;

namespace TrainsModel.Simulation
{
    public class Train
    {
        public RailwayNetwork Map;
        private readonly List<string> Route;

        private readonly int Speed;

        public string LastStation => Route[LastStationInd];
        public string NextStation => LastStationInd != Route.Count - 1 ? Route[LastStationInd + 1] : null;
        private int LastStationInd;

        private int RailwayDistance;
        public int DistanceFromLastStation { get; private set; }

        public bool Arrived => LastStationInd == Route.Count - 1;

        public Train(List<string> route)
        {
            if (route.Count < 2)
                throw new ArgumentException("Minimal route length is 2");

            Route = route;
            Speed = 1;
        }

        public void Start()
        {
            CalcRailwayDistance();
        }

        public void Update(int deltaTime)
        {
            DistanceFromLastStation += Speed * deltaTime;

            if (DistanceFromLastStation == RailwayDistance)
            {
                LastStationInd++;
                DistanceFromLastStation = 0;

                if (Arrived)
                    return;

                CalcRailwayDistance();
            }
        }

        private void CalcRailwayDistance()
        {
            if (!Map.RailwayExist(Route[LastStationInd], Route[LastStationInd + 1]))
                throw new Exception("Not valid train route step: " + Route[LastStationInd + 1]);

            RailwayDistance = Map.GetDistance(Route[LastStationInd], Route[LastStationInd + 1]);
        }

        public void Reset()
        {
            LastStationInd = 0;
            DistanceFromLastStation = 0;
        }
    }
}
