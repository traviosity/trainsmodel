﻿using System;
using System.Collections.Generic;

namespace TrainsModel.Simulation
{
    public class RailwayNetwork
    {
        private class Railway
        {
            public readonly string ToStation;

            public readonly int Distance;

            public Railway(string toStation, int distance)
            {
                ToStation = toStation;
                Distance = distance;
            }
        }

        private readonly Dictionary<string, LinkedList<Railway>> StationsRailways;

        public RailwayNetwork()
        {
            StationsRailways = new Dictionary<string, LinkedList<Railway>>();
        }

        public void AddStation(string name)
        {
            if (StationsRailways.ContainsKey(name))
                throw new Exception(string.Format("Station with name \"{0}\" already exists", name));

            StationsRailways.Add(name, new LinkedList<Railway>());
        }

        public void AddRailway(string name_A, string name_B, int distance)
        {
            if (RailwayExist(name_A, name_B))
                throw new Exception(string.Format("Railway between stations \"{0}\" and \"{1}\" already exists", name_A, name_B));

            if (!StationsRailways.ContainsKey(name_A))
                AddStation(name_A);
            if (!StationsRailways.ContainsKey(name_B))
                AddStation(name_B);

            StationsRailways[name_A].AddLast(new Railway(name_B, distance));
            StationsRailways[name_B].AddLast(new Railway(name_A, distance));
        }

        public bool RailwayExist(string name_A, string name_B)
        {
            if (StationsRailways.ContainsKey(name_A))
            {
                foreach (Railway railway in StationsRailways[name_A])
                    if (railway.ToStation.Equals(name_B))
                        return true;
            }

            return false;
        }

        public int GetDistance(string name_A, string name_B)
        {
            if (StationsRailways.ContainsKey(name_A))
            {
                foreach (Railway railway in StationsRailways[name_A])
                    if (railway.ToStation.Equals(name_B))
                        return railway.Distance;
            }

            throw new Exception(string.Format("Railway between stations \"{0}\" and \"{1}\" does not exists", name_A, name_B));
        }
    }
}
