﻿using JSON;
using System;
using TrainsModel.Simulation;

namespace TrainsModel
{
    public static class RailwayNetworkLoader
    {
        public static RailwayNetwork Load(string source)
        {
            RailwayNetwork result = new RailwayNetwork();

            var data = JSONReader.ReadFile(source);

            if (data.TryGetData("Stations", out JSONDataToRead stations))
            {
                foreach (JSONDataToRead station in stations)
                    result.AddStation(station.GetValue<string>());
            }
            else
                throw new Exception("Can't find stations info in source file");

            if (data.TryGetData("Railways", out JSONDataToRead railways))
            {
                foreach (JSONDataToRead railway in railways)
                {
                    if (!railway.TryGetData("FirstStation", out string firstStation))
                        throw new Exception("Can't find first station info in source file");
                    if (!railway.TryGetData("SecondStation", out string secondStation))
                        throw new Exception("Can't find second station info in source file");
                    if (!railway.TryGetData("Distance", out int distance))
                        throw new Exception("Can't find distance info in source file");

                    result.AddRailway(firstStation, secondStation, distance);
                }
            }
            else
                throw new Exception("Can't find railways info in source file");

            return result;
        }
    }
}
