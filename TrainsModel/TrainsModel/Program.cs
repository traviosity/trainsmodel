﻿using TrainsModel.Simulation;
using System;

namespace TrainsModel
{
    class Program
    {
        static void Main()
        {
            string railsNetworkFileName = "1.json";
            string trainsFileName = "1.json";

            RailwayNetwork map = RailwayNetworkLoader.Load(@"..\..\..\Files\RailwayNetworks\" + railsNetworkFileName);

            RailwaySimulation simulation = new RailwaySimulation(map);

            foreach (Train train in TrainsLoader.Load(@"..\..\..\Files\TrainsCollections\" + trainsFileName))
                simulation.AddTrain(train);

            try
            {
                while (!simulation.AllTrainsArrived)
                    simulation.Update();
            }
            catch (CollisionException)
            {
                Console.WriteLine("Collision detected");
                return;
            }

            Console.WriteLine("All trains arrived successfully");
        }
    }
}
