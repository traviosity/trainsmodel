﻿using JSON;
using System;
using System.Collections.Generic;
using TrainsModel.Simulation;

namespace TrainsModel
{
    public static class TrainsLoader
    {
        public static LinkedList<Train> Load(string source)
        {
            LinkedList<Train> result = new LinkedList<Train>();

            var data = JSONReader.ReadFile(source);

            if (data.TryGetData("Trains", out JSONDataToRead trains))
            {
                foreach (JSONDataToRead train in trains)
                {
                    if (!train.TryGetData("Route", out JSONDataToRead route))
                        throw new Exception("Can't find first station info in source file");

                    List<string> routeList = new List<string>();
                    foreach (JSONDataToRead routeStep in route)
                        routeList.Add(routeStep.GetValue<string>());

                    result.AddLast(new Train(routeList));
                }
            }
            else
                throw new Exception("Can't find railways info in source file");

            return result;
        }
    }
}
